﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace TechHelper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnWikiConvert_OnClick(object sender, RoutedEventArgs e)
        {
            var txtReturn = string.Empty;
            var numLines = TxtWikiInput.LineCount;

            for (var i = 0; i < numLines; i++)
            {
                if (i == 0)
                {
                    txtReturn += string.Format("||{0}||", TxtWikiInput.GetLineText(i).Trim()).Replace("	", "||");
                    txtReturn += Environment.NewLine;
                }
                else
                {
                    txtReturn += string.Format("|{0}|", TxtWikiInput.GetLineText(i).Trim()).Replace("	", "|");
                    txtReturn += Environment.NewLine;
                }
            }

            TxtWikiOutput.Text = txtReturn;
        }

        private void BtnCsvConvert_OnClick(object sender, RoutedEventArgs e)
        {
            var txtReturn = string.Empty;
            var numLines = TxtCsvInput.LineCount;
            var lines = new List<string>();

            for (var i = 0; i < numLines; i++)
            {
                var lineText = TxtCsvInput.GetLineText(i).Trim();
                if (!lines.Exists(x => x == lineText))
                {
                    lines.Add(lineText);
                }
            }

            lines.ForEach(x =>
                {
                    int itemInt;
                    if (int.TryParse(x, out itemInt))
                    {
                        txtReturn += string.Format("{0},", itemInt);
                    }
                    else
                    {
                        txtReturn += string.Format("'{0}',", x);
                    }
                });
            
            TxtCsvOutput.Text = txtReturn.TrimEnd(',');

            TxtCsvOutput.ToolTip = string.Format("{0} out of {1} items copied.", lines.Count, TxtCsvInput.LineCount);
        }
    }
}
